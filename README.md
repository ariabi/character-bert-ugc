# CharacterBert models for North-African Arabizi and French UGC (from the paper "Can Character-based Language Models Improve Downstream TaskPerformances in Low-Resource and Noisy Language Scenarios?")

This repository includes pointers and scripts to reproduce the experiments presented in the paper **"Can Character-based Language Models Improve Downstream Task Performances in Low-Resource and Noisy Language Scenarios?"**  (accepted to the [WNUT workshop](http://noisy-text.github.io/2021/) at EMNLP 2021, [Arxiv preprint here](https://arxiv.org/abs/2110.13658)[6]).

We release the first Character-Bert model (El Boukkouri et al., 2020)[1] trained for French user-generated content and for NArabizi (North-African Arabic dialect written in latin script). The models trained on 1% of Oscar and Narabizi are available [here](https://gitlab.inria.fr/ariabi/character-bert-ugc/-/tree/master/models) (Other models described in the paper are available on demand)

## Pretraining corpora 

**French data:** We used 1% of the French instance of the Oscar corpus collections[4] (2019 dumped) which we used to train the CamemBERT[2] and the characterBert model in our UGC experiments. Available upon request as we used the unshuffled version of French Oscar.

**Narabizi data:** The CharacterBERT model was pretrained on the unlabeled data, 99k sentences, part of the Narabizi treebank[3]. This data set was also used as training data for our MODEL+MLM+TASK experiments.


## Pretraining ChatacterBERT

The code is cloned from [CharacterBERT repository](https://github.com/helboukkouri/character-bert-pretraining) with some modifications (We don't train the model for Next Sentence Prediction (NSP), the preproccesing and training code were changed accordingly). Details for the preproccesing can be found in the README file ([Pretraining](https://gitlab.inria.fr/ariabi/character-bert-ugc/-/blob/master/code/pretraining-code/README.md) / [Fine Tuning](https://gitlab.inria.fr/ariabi/character-bert-ugc/-/blob/master/code/finetuning-code/README.md)).

Command to train Narabizi ChatacterBERT 

```
export WORKDIR="."

export NUM_GPUs=3

CORPUS="arabizi"
MODEL_TYPE="character_bert"
export INPUT_DIR="$WORKDIR/data/hdf5/$CORPUS/$MODEL_TYPE/128_20/"
export OUTPUT_DIR="$WORKDIR/output_directory/$CORPUS/$MODEL_TYPE/"
export EXPERIMENT_NAME=$MODEL_TYPE"_128_20"

CMD="$WORKDIR/pretrain_model.py"
CMD+=" --hdf5_directory=$INPUT_DIR"
CMD+=" --output_directory=$OUTPUT_DIR"
CMD+=" --tensorboard_id=$EXPERIMENT_NAME"
CMD+=" --max_input_length=128"
CMD+=" --max_masked_tokens_per_input=20"
CMD+=" --target_batch_size=8192"
CMD+=" --num_accumulation_steps=512"
CMD+=" --learning_rate=6e-3"
CMD+=" --warmup_proportion=0.2843"
CMD+=" --total_steps=3000"
CMD+=" --fp16"
CMD+=" --allreduce_post_accumulation"
CMD+=" --allreduce_post_accumulation_fp16"
CMD+=" --is_character_bert"
CMD+=" --do_validation"
CMD+=" --random_seed=42"

python -m torch.distributed.launch \
  --nproc_per_node=$NUM_GPUs \
  $CMD
```

Command to train 1% Oscar ChatacterBERT

```
export WORKDIR="."

export NUM_GPUs=3

CORPUS="oscar_1percent"
MODEL_TYPE="character_bert"
export INPUT_DIR="$WORKDIR/data/hdf5/$CORPUS/$MODEL_TYPE/128_20/"
export OUTPUT_DIR="$WORKDIR/output_directory/$CORPUS/${MODEL_TYPE}_Only_MLM-20k/"
export EXPERIMENT_NAME=$MODEL_TYPE"_"$CORPUS"_128_20_Only_MLM-20k"

CMD="$WORKDIR/pretrain_model.py"
CMD+=" --hdf5_directory=$INPUT_DIR"
CMD+=" --output_directory=$OUTPUT_DIR"
CMD+=" --tensorboard_id=$EXPERIMENT_NAME"
CMD+=" --max_input_length=128"
CMD+=" --max_masked_tokens_per_input=20"
CMD+=" --target_batch_size=8192"
CMD+=" --num_accumulation_steps=512"
CMD+=" --learning_rate=6e-3"
CMD+=" --warmup_proportion=0.2843"
CMD+=" --total_steps=20000"
CMD+=" --fp16"
CMD+=" --allreduce_post_accumulation"
CMD+=" --allreduce_post_accumulation_fp16"
CMD+=" --is_character_bert"
CMD+=" --do_validation"
CMD+=" --random_seed=42 --Only_MLM"

python -m torch.distributed.launch \
  --nproc_per_node=$NUM_GPUs \
  $CMD
```

##  Dependency Parsing Fine Tuning
We used the parser from [npdependency repository](https://github.com/LoicGrobol/npdependency) [5].
```
CONFIGFILE=examples/path-to-config-file
MODEL=characterBert_arabizi

#Fine tuning
python -u -m npdependency.graph_parser  --train_file train.NArabizi_treebank.conllu --dev_file dev.NArabizi_treebank.conllu --out_dir output/$MODEL --overwrite $CONFIGFILE --overwrite --device cuda:0

#Parsing
hopsparser parse output/$MODEL/model test.NArabizi_treebank.conllu output/$MODEL/test.conllu
```

Details of the Python environments to use for both steps can be found in the requirements files and joined README.

##  Models
We share the pretrained and finetuned models for the main experiments, other models are available on demand.
## How to cite

If you use this work, please cite both papers in the following way "We used the CharacterBert model \cite{el-boukkouri-etal-2020-characterbert} as instanced to {Narabizi,French} by \citet{riabi-2021-can}.

```
@inproceedings{riabi-2021-can,
    title = "Can Character-based Language Models Improve Downstream Task Performances in Low-Resource and Noisy Language Scenarios?",
    author = {Arij Riabi and and Beno{\^{\i}}t Sagot and Djam{\'{e}} Seddah},
    booktitle = "Proceedings of the Seventh Workshop on Noisy User-generated Text (W-NUT 2021)",
    month = nov,
    year = "2021",
    address = "Punta Cana, Dominican Republic (Online)",
    publisher = "Association for Computational Linguistics",
}
```

```
@inproceedings{el-boukkouri-etal-2020-characterbert,
    title = "{C}haracter{BERT}: Reconciling {ELM}o and {BERT} for Word-Level Open-Vocabulary Representations From Characters",
    author = "El Boukkouri, Hicham  and
      Ferret, Olivier  and
      Lavergne, Thomas  and
      Noji, Hiroshi  and
      Zweigenbaum, Pierre  and
      Tsujii, Jun{'}ichi",
    booktitle = "Proceedings of the 28th International Conference on Computational Linguistics",
    month = dec,
    year = "2020",
    address = "Barcelona, Spain (Online)",
    publisher = "International Committee on Computational Linguistics",
    url = "https://www.aclweb.org/anthology/2020.coling-main.609",
    doi = "10.18653/v1/2020.coling-main.609",
    pages = "6903--6915",
    abstract = "Due to the compelling improvements brought by BERT, many recent representation models adopted the Transformer architecture as their main building block, consequently inheriting the wordpiece tokenization system despite it not being intrinsically linked to the notion of Transformers. While this system is thought to achieve a good balance between the flexibility of characters and the efficiency of full words, using predefined wordpiece vocabularies from the general domain is not always suitable, especially when building models for specialized domains (e.g., the medical domain). Moreover, adopting a wordpiece tokenization shifts the focus from the word level to the subword level, making the models conceptually more complex and arguably less convenient in practice. For these reasons, we propose CharacterBERT, a new variant of BERT that drops the wordpiece system altogether and uses a Character-CNN module instead to represent entire words by consulting their characters. We show that this new model improves the performance of BERT on a variety of medical domain tasks while at the same time producing robust, word-level, and open-vocabulary representations.",
}
```
## References
[1] Hicham El Boukkouri, Olivier Ferret, Thomas Lavergne, Hiroshi Noji, Pierre Zweigenbaum, and Jun’ichi Tsujii. 2020. [CharacterBERT: Reconciling ELMo  and  BERT  for  word-level  open-vocabulary representations from characters](https://aclanthology.org/2020.coling-main.609/). In Proceedings of the 28th International Conference on Computational Linguistics pages 6903–6915, Barcelona, Spain (Online). International Committee on Computational Linguistics.

[2] Louis Martin, Benjamin Muller, Pedro Javier Ortiz Suárez, Yoann Dupont, Laurent Romary, Éric de la Clergerie, Djamé Seddah, and Benoît Sagot. 2020 [CamemBERT: a tasty French language model](https://camembert-model.fr). In Proceedings of the 58th Annual Meeting of the Association for Computational Linguistics, pages 7203–7219, Online. Association for Computational Linguistics.

[3] Djamé Seddah, Farah Essaidi, Amal Fethi, Matthieu Futeral, Benjamin Muller, Pedro Javier Ortiz Suárez, Benoît Sagot, and Abhishek Srivastava. 2020b [Building a User-Generated Content North-African Arabizi Teebank: Tackling Hell](https://aclanthology.org/2020.acl-main.107/). In Proceedings of the 58th Annual Meeting of the Association for Computational Linguistics, pages 1139–1150.

[4] Pedro Javier Ortiz Suárez, Benoît Sagot, and Laurent Romary. 2019. [Asynchronous pipeline for processing huge corpora on medium to low resource infras- tructures](https://oscar-corpus.com). In 7th Workshop on the Challenges in the Management of Large Corpora (CMLC-7). Leibniz- Institut für Deutsche Sprache.

[5] Loïc  Grobol  and  Benoît  Crabbé. 2021. [Analyse en dépendances du français avec des plongements contextualisés](https://hal.archives-ouvertes.fr/hal-03223424). In Actes de la 28ème Conférence sur le Traitement Automatique des Langues Naturelles. Lille, France.

[6] Arij Riabi, Benoît Sagot, Djamé Seddah. 2021. [Can Character-based Language Models Improve Downstream Task Performance in Low-Resource and Noisy Language Scenarios?](https://arxiv.org/abs/2110.13658). In Proceedings of the 7th Workshop on Noisy User-generated Text (W-NUT), colocated with EMNLP 2021. Punta Cana, Domican Republic.
