import torch
import argparse
from transformers import CharacterBertConfig, CharacterBertForMaskedLM,CharacterBertTokenizer



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--checkpoint')
    parser.add_argument('--model_save')
    # parser.add_argument('--mlm_vocab_file')
    args = parser.parse_args()

    orig_model = torch.load(args.checkpoint,map_location=torch.device('cpu'))
    orig_state_dict = orig_model["model"]
    config = CharacterBertConfig.from_pretrained("./data/character-bert")

    #save tokenizer
    # tokenizer = CharacterBertTokenizer.from_pretrained("helboukkouri/character-bert",mlm_vocab_file=args.mlm_vocab_file)
    # tokenizer.save_pretrained(args.model_save)

    #save model
    model = CharacterBertForMaskedLM(config)
    model.load_state_dict(orig_model["model"], strict=True)
    model.half()
    model.save_pretrained(args.model_save)
    print('finished')

if __name__ == "__main__":
    main()
# python convert_checkpoint.py --checkpoint output_directory/arabizi/character_bert_Only_MLM/ckpt_793.pt --model_save models/characterBert_arabizi

# python convert_checkpoint.py --checkpoint output_directory/arabizi+oscar/character_bert_Only_MLM/ckpt_794.pt --model_save models/characterBert_arabizi+oscar

# python convert_checkpoint.py --checkpoint output_directory/sample_oscar/character_bert_Only_MLM/ckpt_791.pt --model_save models/characterBert_sample_oscar

# python convert_checkpoint.py --checkpoint output_directory/66arabizi+33oscar/character_bert_Only_MLM/ckpt_794.pt --model_save models/characterBert_70arabizi+30oscar
#awk '{ print $2 }' data/mlm_vocabularies/66arabizi+33oscar/mlm_vocab.txt > models/characterBert_70arabizi+30oscar/mlm_vocab.txt
# python convert_checkpoint.py --checkpoint output_directory/oscar_1percent/character_bert_Only_MLM-20k/ckpt_17384.pt --model_save models/characterBert_1percent_oscar-17ksteps
#awk '{ print $2 }' data/mlm_vocabularies/oscar_1percent/mlm_vocab.txt > models/characterBert_1percent_oscar-17ksteps/mlm_vocab.txt