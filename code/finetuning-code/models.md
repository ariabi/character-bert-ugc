# Pretrained models

Sorted by language and corpus.

**Usage note**: The -camembert and -flaubert models use the eponymous embeddings and as such put a
relatively heavy load on hardware. We recommend using them on GPUs with at least 10 GiB memory. Otherwise,
running them on CPUs is still possible, albeit slow.

## French

### FTB-UD

| Model name | UPOS (dev) | LAS (dev) | UPOS (test) | LAS (test) | Download |
|:-----------|:----------:|:---------:|:-----------:|:----------:|:--------:|
| UD_French-FTB-2.7-camembert | 98.46 | 88.23 | 98.57 | 88.43 | [link][UD_French-FTB-2.7-camembert] |
| UD_French-FTB-2.7-flaubert | 98.48 | 88.56 | 98.57 | 88.45 | [link][UD_French-FTB-2.7-flaubert] |
| UD_French-FTB-2.7-nobert | 97.83 | 84.54 | 97.85 | 83.77 | [link][UD_French-FTB-2.7-nobert] |

[UD_French-FTB-2.7-camembert]: https://sharedocs.huma-num.fr/wl/?id=bSBG4wDnjGiiQiMUaMoBverIc5WE7Uox&fmode=download

[UD_French-FTB-2.7-flaubert]: https://sharedocs.huma-num.fr/wl/?id=DuantRUPLGlpFqdFFmYJ7UcQarp4G35N&fmode=download

[UD_French-FTB-2.7-nobert]: https://sharedocs.huma-num.fr/wl/?id=tcYFkTG3QXh1nSkQL69DAUJxBLHx4hdf&fmode=download

### FTB-SPMRL

| Model name | UPOS (dev) | LAS (dev) | UPOS (test) | LAS (test) | Download |
|:-----------|:----------:|:---------:|:-----------:|:----------:|:--------:|
| ftb_spmrl-camembert | 98.83 | 89.07 | 98.74 | 89.39 | [link][ftb_spmrl-camembert] |
| ftb_spmrl-flaubert | 98.88 | 89.43 | 98.78 | 89.54 | [link][ftb_spmrl-flaubert] |
| ftb_spmrl-nobert | 98.17 | 85.46 | 98.03 | 84.54 | [link][ftb_spmrl-nobert] |

[ftb_spmrl-camembert]: https://sharedocs.huma-num.fr/wl/?id=WDQ8A8bsSdbMCHmLGB4PA9P551WMFL9v&fmode=download

[ftb_spmrl-flaubert]: https://sharedocs.huma-num.fr/wl/?id=YuJjhXWP7UORLtTKwrtOxLQnNWOpmkTY&fmode=download

[ftb_spmrl-nobert]: https://sharedocs.huma-num.fr/wl/?id=igaumRaYZWeDjPgaOKk74su3Ey7GIHT8&fmode=download

### GSD-UD

| Model name | UPOS (dev) | LAS (dev) | UPOS (test) | LAS (test) | Download |
|:-----------|:----------:|:---------:|:-----------:|:----------:|:--------:|
| UD_French-GSD-2.7-camembert | 98.53 | 95.56 | 98.27 | 93.93 | [link][UD_French-GSD-2.7-camembert] |
| UD_French-GSD-2.7-flaubert | 98.59 | 95.56 | 98.56 | 94.19 | [link][UD_French-GSD-2.7-flaubert] |
| UD_French-GSD-2.7-nobert | 97.77 | 91.63 | 97.24 | 89.04 | [link][UD_French-GSD-2.7-nobert] |

[UD_French-GSD-2.7-camembert]: https://sharedocs.huma-num.fr/wl/?id=WvuQmIe19ezwXLVhamQOLIhis1Ifwaw8&fmode=download

[UD_French-GSD-2.7-flaubert]: https://sharedocs.huma-num.fr/wl/?id=WuJal5961Vng83Er90gkVC9LGBSp4iqX&fmode=download

[UD_French-GSD-2.7-nobert]: https://sharedocs.huma-num.fr/wl/?id=WREEhUZMu7C8WrQVnqL9gnKJttZAiG97&fmode=download

### Sequoia-UD

| Model name | UPOS (dev) | LAS (dev) | UPOS (test) | LAS (test) | Download |
|:-----------|:----------:|:---------:|:-----------:|:----------:|:--------:|
| UD_French-Sequoia-2.7-camembert | 98.92 | 93.67 | 99.22 | 93.77 | [link][UD_French-Sequoia-2.7-camembert] |
| UD_French-Sequoia-2.7-flaubert | 99.21 | 94.09 | 99.36 | 94.40 | [link][UD_French-Sequoia-2.7-flaubert] |
| UD_French-Sequoia-2.7-nobert | 96.82 | 84.60 | 97.14 | 84.73 | [link][UD_French-Sequoia-2.7-nobert] |

[UD_French-Sequoia-2.7-camembert]: https://sharedocs.huma-num.fr/wl/?id=Xf2yTDk3rkfUtzDRcTCmGI1fuJ6p6qup&fmode=download

[UD_French-Sequoia-2.7-flaubert]: https://sharedocs.huma-num.fr/wl/?id=1QniPAnJQeoRWhpSPI7cPFsTN1y78Twp&fmode=download

[UD_French-Sequoia-2.7-nobert]: https://sharedocs.huma-num.fr/wl/?id=yiq7c9tYIAxBQmMOJVLFrtbHxkZwa1Xu&fmode=download

### French-Spoken-UD

| Model name | UPOS (dev) | LAS (dev) | UPOS (test) | LAS (test) | Download |
|:-----------|:----------:|:---------:|:-----------:|:----------:|:--------:|
| UD_French-Spoken-2.7-camembert | 96.20 | 80.76 | 95.86 | 78.97 | [link][UD_French-Spoken-2.7-camembert] |
| UD_French-Spoken-2.7-flaubert | 96.85 | 81.45 | 96.75 | 80.46 | [link][UD_French-Spoken-2.7-flaubert] |
| UD_French-Spoken-2.7-nobert | 93.53 | 71.46 | 92.62 | 69.78 | [link][UD_French-Spoken-2.7-nobert] |

[UD_French-Spoken-2.7-camembert]: https://sharedocs.huma-num.fr/wl/?id=mAHijt9mBTs0bfy1jH9nHPOOp1Spx0Rh&fmode=download

[UD_French-Spoken-2.7-flaubert]: https://sharedocs.huma-num.fr/wl/?id=kyvgnfyhu41RBDK70cUCaSrO9gkRPBr3&fmode=download

[UD_French-Spoken-2.7-nobert]: https://sharedocs.huma-num.fr/wl/?id=FNQY4UWTP9NsFNsAAgEpbMfHyzdmzFIM&fmode=download

## Old French

### SRCMF-UD

⚠ These models are released as previews and have not yet been as extensively tested

| Model name | UPOS (dev) | LAS (dev) | UPOS (test) | LAS (test) | Download |
|:-----------|:----------:|:---------:|:-----------:|:----------:|:--------:|
| UD_Old_French-SRCMF-flaubert+fro | 97.15 | 90.24 | 97.22 | 90.39 | [link][UD_Old_French-SRCMF-flaubert+fro] |

[UD_Old_French-SRCMF-flaubert+fro]: https://sharedocs.huma-num.fr/wl/?id=ssFXOn4ms2ZYx36Xe0FHfaXU1YKoXIA1&fmode=download